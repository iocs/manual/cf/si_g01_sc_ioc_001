#
# Module: essioc
#
require essioc
require modbus

#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")


#
# IOC: SI-G01:Ctrl-IOC-01
# Load .iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/si_g01.iocsh", "DEVICENAME=SI-G01:, IPADDR=172.17.15.16, MODBUSDRVPORT=502, RECVTIMEOUT=5000")
